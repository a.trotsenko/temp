package com.haulmont.cuba.core.sys.persistence;

import org.eclipse.persistence.internal.sessions.AbstractSession;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author at
 */
public class CubaHanaPlatform extends HANAPlatform {

    static final String UUID_REG_EXP = "\\b[0-9a-f]{8}\\b-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-\\b[0-9a-f]{12}\\b";

    private boolean isUUID(Object o) {
        return o != null && o instanceof String && o.toString().matches(UUID_REG_EXP);
    }

    private String fixUUID(Object uuid) {
        return uuid.toString().replaceAll("-", "");
    }

    @Override
    public Object convertToDatabaseType(Object value) {
        if (isUUID(value)) {
            return fixUUID(value);
        }
        return super.convertToDatabaseType(value);
    }

    @Override
    public void setParameterValueInDatabaseCall(Object parameter, PreparedStatement statement, int index, AbstractSession session) throws SQLException {
        if (isUUID(parameter)) {
            parameter = fixUUID(parameter);
        }
        super.setParameterValueInDatabaseCall(parameter, statement, index, session);
    }
}
