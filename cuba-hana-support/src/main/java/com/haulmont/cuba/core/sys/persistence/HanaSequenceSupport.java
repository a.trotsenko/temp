package com.haulmont.cuba.core.sys.persistence;

/**
 * @author at
 */
public class HanaSequenceSupport implements SequenceSupport {
    @Override
    public String sequenceExistsSql(String sequenceName) {
        return "select name from \"SYS\".\"P_SEQUENCES_\" where name = '" + sequenceName + "'";
    }

    @Override
    public String createSequenceSql(String sequenceName, long startValue, long increment) {
        return "create sequence \"" + sequenceName + "\" start with " + startValue + " increment by " + increment;
    }

    @Override
    public String modifySequenceSql(String sequenceName, long startWith) {
        return "ALTER SEQUENCE " + sequenceName + " RESTART WITH " + startWith;
    }

    @Override
    public String deleteSequenceSql(String sequenceName) {
        return " DROP SEQUENCE " + sequenceName;
    }

    @Override
    public String getNextValueSql(String sequenceName) {
        return "SELECT " + sequenceName + ".NEXTVAL FROM DUMMY";
    }

    @Override
    public String getCurrentValueSql(String sequenceName) {
        return "SELECT " + sequenceName + ".CURRVAL FROM DUMMY";
    }
}
