create table SYS_SERVER (
    ID varchar(32),
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    NAME varchar(190),
    IS_RUNNING varchar(1),
    DATA text,
    primary key (ID)
)^

create unique index IDX_SYS_SERVER_UNIQ_NAME on SYS_SERVER (NAME)^

create table SYS_CONFIG (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    --
    NAME nvarchar(190) not null, -- See PL-8935, 190 length is set due to MySQL problem with encoding
    VALUE_ text not null,
    --
    primary key (ID)
)^

create unique index IDX_SYS_CONFIG_UNIQ_NAME on SYS_CONFIG (NAME)^

create table SYS_FILE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    NAME nvarchar(500) not null,
    EXT nvarchar(20),
    FILE_SIZE bigint,
    CREATE_DATE timestamp,
    --
    primary key (ID)
)^



create table SYS_LOCK_CONFIG (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    NAME nvarchar(100),
    TIMEOUT_SEC integer,
    --
    primary key (ID)
)^



create table SYS_ENTITY_STATISTICS (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    --
    NAME nvarchar(50),
    INSTANCE_COUNT bigint,
    FETCH_UI integer,
    MAX_FETCH_UI integer,
    LAZY_COLLECTION_THRESHOLD integer,
    LOOKUP_SCREEN_THRESHOLD integer,
    --
    primary key (ID)
)^

create unique index IDX_SYS_ENTITY_STATISTICS_UNIQ_NAME on SYS_ENTITY_STATISTICS (NAME)^

create table SYS_SCHEDULED_TASK (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    DEFINED_BY nvarchar(1) default 'B',
    CLASS_NAME nvarchar(500),
    SCRIPT_NAME nvarchar(500),
    BEAN_NAME nvarchar(50),
    METHOD_NAME nvarchar(50),
    METHOD_PARAMS nvarchar(1000),
    USER_NAME nvarchar(50),
    IS_SINGLETON varchar(1),
    IS_ACTIVE varchar(1),
    PERIOD integer,
    TIMEOUT integer,
    START_DATE timestamp,
    TIME_FRAME integer,
    START_DELAY integer,
    PERMITTED_SERVERS nvarchar(4096),
    LOG_START varchar(1),
    LOG_FINISH varchar(1),
    LAST_START_TIME timestamp,
    LAST_START_SERVER nvarchar(512),
    DESCRIPTION nvarchar(1000),
    CRON nvarchar(100),
    SCHEDULING_TYPE nvarchar(1) default 'P',
    --
    primary key (ID)
)^

-- create unique index IDX_SYS_SCHEDULED_TASK_UNIQ_BEAN_METHOD on SYS_SCHEDULED_TASK (BEAN_NAME, METHOD_NAME, METHOD_PARAMS, DELETE_TS)^



create table SYS_SCHEDULED_EXECUTION (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    TASK_ID nvarchar(32),
    SERVER nvarchar(512),
    START_TIME timestamp,
    FINISH_TIME timestamp,
    RESULT text,
    --
    primary key (ID),
    constraint SYS_SCHEDULED_EXECUTION_TASK foreign key (TASK_ID) references SYS_SCHEDULED_TASK(ID)
)^

create index IDX_SYS_SCHEDULED_EXECUTION_TASK_START_TIME  on SYS_SCHEDULED_EXECUTION (TASK_ID, START_TIME)^
create index IDX_SYS_SCHEDULED_EXECUTION_TASK_FINISH_TIME on SYS_SCHEDULED_EXECUTION (TASK_ID, FINISH_TIME)^


create table SEC_ROLE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    NAME nvarchar(190) not null,
    LOC_NAME nvarchar(255),
    DESCRIPTION nvarchar(1000),
    IS_DEFAULT_ROLE varchar(1),
    ROLE_TYPE integer,
    --
    primary key (ID)
)^

create unique index IDX_SEC_ROLE_UNIQ_NAME on SEC_ROLE (NAME, DELETE_TS_NN)^




create table SEC_GROUP (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    NAME nvarchar(190) not null,
    PARENT_ID nvarchar(32),
    --
    primary key (ID),
    constraint SEC_GROUP_PARENT foreign key (PARENT_ID) references SEC_GROUP(ID)
)^

create unique index IDX_SEC_GROUP_UNIQ_NAME on SEC_GROUP (NAME, DELETE_TS_NN)^



create table SEC_GROUP_HIERARCHY (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    GROUP_ID nvarchar(32),
    PARENT_ID nvarchar(32),
    HIERARCHY_LEVEL integer,
    --
    primary key (ID),
    constraint SEC_GROUP_HIERARCHY_GROUP foreign key (GROUP_ID) references SEC_GROUP(ID),
    constraint SEC_GROUP_HIERARCHY_PARENT foreign key (PARENT_ID) references SEC_GROUP(ID)
)^



create table SEC_USER (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    LOGIN nvarchar(50) not null,
    LOGIN_LC nvarchar(50) not null,
    PASSWORD nvarchar(255),
    NAME nvarchar(255),
    FIRST_NAME nvarchar(255),
    LAST_NAME nvarchar(255),
    MIDDLE_NAME nvarchar(255),
    POSITION_ nvarchar(255),
    EMAIL nvarchar(100),
    LANGUAGE_ nvarchar(20),
    TIME_ZONE nvarchar(50),
    TIME_ZONE_AUTO varchar(1),
    ACTIVE char(1),
    GROUP_ID nvarchar(32) not null,
    IP_MASK nvarchar(200),
    CHANGE_PASSWORD_AT_LOGON varchar(1),
    --
    primary key (ID),
    constraint SEC_USER_GROUP foreign key (GROUP_ID) references SEC_GROUP(ID)
)^

create unique index IDX_SEC_USER_UNIQ_LOGIN on SEC_USER (LOGIN_LC, DELETE_TS_NN)^


create table SEC_USER_ROLE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    USER_ID nvarchar(32),
    ROLE_ID nvarchar(32),
    --
    primary key (ID),
    constraint SEC_USER_ROLE_PROFILE foreign key (USER_ID) references SEC_USER(ID),
    constraint SEC_USER_ROLE_ROLE foreign key (ROLE_ID) references SEC_ROLE(ID)
)^

create unique index IDX_SEC_USER_ROLE_UNIQ_ROLE on SEC_USER_ROLE (USER_ID, ROLE_ID, DELETE_TS_NN)^



create table SEC_PERMISSION (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    PERMISSION_TYPE integer,
    TARGET nvarchar(100),
    VALUE_ integer,
    ROLE_ID nvarchar(32),
    --
    primary key (ID),
    constraint SEC_PERMISSION_ROLE foreign key (ROLE_ID) references SEC_ROLE(ID)
)^

create unique index IDX_SEC_PERMISSION_UNIQUE on SEC_PERMISSION (ROLE_ID, PERMISSION_TYPE, TARGET, DELETE_TS_NN)^



create table SEC_CONSTRAINT (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    CODE nvarchar(255),
    CHECK_TYPE nvarchar(50) default 'db',
    OPERATION_TYPE nvarchar(50) default 'read',
    ENTITY_NAME nvarchar(255) not null,
    JOIN_CLAUSE nvarchar(500),
    WHERE_CLAUSE nvarchar(1000),
    GROOVY_SCRIPT text,
    FILTER_XML text,
    IS_ACTIVE varchar(1) default 1,
    GROUP_ID nvarchar(32),
    --
    primary key (ID),
    constraint SEC_CONSTRAINT_GROUP foreign key (GROUP_ID) references SEC_GROUP(ID)
)^



create table SEC_LOCALIZED_CONSTRAINT_MSG (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    ENTITY_NAME nvarchar(150) not null,
    OPERATION_TYPE nvarchar(40) not null,
    VALUES_ text,
    --
    primary key (ID)
)^

create unique index IDX_SEC_LOC_CNSTRNT_MSG_UNIQUE
  on SEC_LOCALIZED_CONSTRAINT_MSG (ENTITY_NAME, OPERATION_TYPE, DELETE_TS_NN)^


create table SEC_SESSION_ATTR (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    NAME nvarchar(50),
    STR_VALUE nvarchar(1000),
    DATATYPE nvarchar(20),
    GROUP_ID nvarchar(32),
    --
    primary key (ID),
    constraint SEC_SESSION_ATTR_GROUP foreign key (GROUP_ID) references SEC_GROUP(ID)
)^



create table SEC_USER_SETTING (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    USER_ID nvarchar(32),
    CLIENT_TYPE char(1),
    NAME nvarchar(190),
    VALUE_ text,
    --
    primary key (ID),
    constraint SEC_USER_SETTING_USER foreign key (USER_ID) references SEC_USER(ID),
    constraint SEC_USER_SETTING_UNIQ unique (USER_ID, NAME, CLIENT_TYPE)
)^



create table SEC_USER_SUBSTITUTION (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    USER_ID nvarchar(32) not null,
    SUBSTITUTED_USER_ID nvarchar(32) not null,
    START_DATE timestamp,
    END_DATE timestamp,
    --
    primary key (ID),
    constraint FK_SEC_USER_SUBSTITUTION_USER foreign key (USER_ID) references SEC_USER(ID),
    constraint FK_SEC_USER_SUBSTITUTION_SUBSTITUTED_USER foreign key (SUBSTITUTED_USER_ID) references SEC_USER(ID)
)^



create table SEC_LOGGED_ENTITY (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    NAME nvarchar(100),
    AUTO varchar(1),
    MANUAL varchar(1),
    --
    primary key (ID),
    constraint SEC_LOGGED_ENTITY_UNIQ_NAME unique (NAME)
)^



create table SEC_LOGGED_ATTR (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    ENTITY_ID nvarchar(32),
    NAME nvarchar(50),
    --
    primary key (ID),
    constraint FK_SEC_LOGGED_ATTR_ENTITY foreign key (ENTITY_ID) references SEC_LOGGED_ENTITY(ID),
    constraint SEC_LOGGED_ATTR_UNIQ_NAME unique (ENTITY_ID, NAME)
)^



create table SEC_ENTITY_LOG (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    EVENT_TS timestamp,
    USER_ID nvarchar(32),
    CHANGE_TYPE char(1),
    ENTITY nvarchar(100),
    ENTITY_ID nvarchar(32),
    STRING_ENTITY_ID nvarchar(190),
    INT_ENTITY_ID integer,
    LONG_ENTITY_ID bigint,
    CHANGES text,
    --
    primary key (ID),
    constraint FK_SEC_ENTITY_LOG_USER foreign key (USER_ID) references SEC_USER(ID)
)^

create index IDX_SEC_ENTITY_LOG_ENTITY_ID on SEC_ENTITY_LOG (ENTITY_ID)^
create index IDX_SEC_ENTITY_LOG_SENTITY_ID on SEC_ENTITY_LOG (STRING_ENTITY_ID)^
create index IDX_SEC_ENTITY_LOG_IENTITY_ID on SEC_ENTITY_LOG (INT_ENTITY_ID)^
create index IDX_SEC_ENTITY_LOG_LENTITY_ID on SEC_ENTITY_LOG (LONG_ENTITY_ID)^



create table SEC_FILTER (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    COMPONENT nvarchar(200),
    NAME nvarchar(255),
    CODE nvarchar(200),
    XML text,
    USER_ID nvarchar(32),
    GLOBAL_DEFAULT varchar(1),
    --
    primary key (ID),
    constraint FK_SEC_FILTER_USER foreign key (USER_ID) references SEC_USER(ID)
)^



create table SYS_FOLDER (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    FOLDER_TYPE char(1),
    PARENT_ID nvarchar(32),
    NAME nvarchar(100),
    TAB_NAME nvarchar(100),
    SORT_ORDER integer,
    --
    primary key (ID),
    constraint FK_SYS_FOLDER_PARENT foreign key (PARENT_ID) references SYS_FOLDER(ID)
)^



create table SYS_APP_FOLDER (
    FOLDER_ID nvarchar(32),
    FILTER_COMPONENT nvarchar(200),
    FILTER_XML nvarchar(5000),
    VISIBILITY_SCRIPT text,
    QUANTITY_SCRIPT text,
    APPLY_DEFAULT varchar(1),
    --
    primary key (FOLDER_ID),
    constraint FK_SYS_APP_FOLDER_FOLDER foreign key (FOLDER_ID) references SYS_FOLDER(ID)
)^



create table SEC_PRESENTATION (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    --
    COMPONENT nvarchar(200),
    NAME nvarchar(255),
    XML nvarchar(5000),
    USER_ID nvarchar(32),
    IS_AUTO_SAVE varchar(1),
    --
    primary key (ID),
    constraint SEC_PRESENTATION_USER foreign key (USER_ID) references SEC_USER(ID)
)^



create table SEC_SEARCH_FOLDER (
    FOLDER_ID nvarchar(32),
    FILTER_COMPONENT nvarchar(200),
    FILTER_XML nvarchar(5000),
    USER_ID nvarchar(32),
    PRESENTATION_ID nvarchar(32),
    APPLY_DEFAULT varchar(1),
    IS_SET varchar(1),
    ENTITY_TYPE nvarchar(50),
    --
    primary key (FOLDER_ID),
    constraint FK_SEC_SEARCH_FOLDER_FOLDER foreign key (FOLDER_ID) references SYS_FOLDER(ID),
    constraint FK_SEC_SEARCH_FOLDER_USER foreign key (USER_ID) references SEC_USER(ID),
    constraint FK_SEC_SEARCH_FOLDER_PRESENTATION foreign key (PRESENTATION_ID)
        references SEC_PRESENTATION(ID)
        on delete set null
)^

create index IDX_SEC_SEARCH_FOLDER_USER on SEC_SEARCH_FOLDER (USER_ID)^



create table SYS_FTS_QUEUE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    ENTITY_ID nvarchar(32),
    STRING_ENTITY_ID nvarchar(190),
    INT_ENTITY_ID integer,
    LONG_ENTITY_ID bigint,
    ENTITY_NAME nvarchar(200),
    CHANGE_TYPE char(1),
    SOURCE_HOST nvarchar(255),
    INDEXING_HOST nvarchar(190),
    FAKE varchar(1),
    --
    primary key (ID)
)^

create index IDX_SYS_FTS_QUEUE_IDXHOST_CRTS on SYS_FTS_QUEUE (INDEXING_HOST, CREATE_TS)^



create table SEC_SCREEN_HISTORY (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    USER_ID nvarchar(32),
    CAPTION nvarchar(255),
    URL text,
    ENTITY_ID nvarchar(32),
    SUBSTITUTED_USER_ID nvarchar(32),
      --
    primary key (ID),
    constraint FK_SEC_HISTORY_USER foreign key (USER_ID) references SEC_USER (ID),
    constraint FK_SEC_HISTORY_SUBSTITUTED_USER foreign key (SUBSTITUTED_USER_ID) references SEC_USER (ID)
)^

create index IDX_SEC_SCREEN_HISTORY_USER on SEC_SCREEN_HISTORY (USER_ID)^
create index IDX_SEC_SCREEN_HIST_SUB_USER on SEC_SCREEN_HISTORY (SUBSTITUTED_USER_ID)^



create table SYS_SENDING_MESSAGE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    ADDRESS_TO text,
    ADDRESS_FROM nvarchar(100),
    CAPTION nvarchar(500),
    EMAIL_HEADERS nvarchar(500),
    CONTENT_TEXT text,
    CONTENT_TEXT_FILE_ID nvarchar(32),
    DEADLINE timestamp,
    STATUS int,
    DATE_SENT timestamp,
    ATTEMPTS_COUNT int,
    ATTEMPTS_MADE int,
    ATTACHMENTS_NAME text,
    BODY_CONTENT_TYPE nvarchar(50),
    --
    primary key (ID),
    constraint FK_SYS_SENDING_MESSAGE_CONTENT_FILE foreign key (CONTENT_TEXT_FILE_ID) references SYS_FILE(ID)
)^

create index IDX_SYS_SENDING_MESSAGE_STATUS on SYS_SENDING_MESSAGE (STATUS)^

create index IDX_SYS_SENDING_MESSAGE_DATE_SENT on SYS_SENDING_MESSAGE (DATE_SENT)^

create index IDX_SYS_SENDING_MESSAGE_UPDATE_TS on SYS_SENDING_MESSAGE (UPDATE_TS)^


create table SYS_SENDING_ATTACHMENT (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    MESSAGE_ID nvarchar(32),
    CONTENT blob,
    CONTENT_FILE_ID nvarchar(32),
    CONTENT_ID nvarchar(50),
    NAME nvarchar(500),
    DISPOSITION nvarchar(50),
    TEXT_ENCODING nvarchar(50),

    --
    primary key (ID),
    constraint FK_SYS_SENDING_ATTACHMENT_SENDING_MESSAGE foreign key (MESSAGE_ID) references SYS_SENDING_MESSAGE (ID),
    constraint FK_SYS_SENDING_ATTACHMENT_CONTENT_FILE foreign key (CONTENT_FILE_ID) references SYS_FILE (ID)
)^

create index SYS_SENDING_ATTACHMENT_MESSAGE_IDX on SYS_SENDING_ATTACHMENT (MESSAGE_ID)^



create table SYS_ENTITY_SNAPSHOT (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    --
    ENTITY_META_CLASS nvarchar(50) not null,
    ENTITY_ID nvarchar(32),
    STRING_ENTITY_ID nvarchar(190),
    INT_ENTITY_ID integer,
    LONG_ENTITY_ID bigint,
    AUTHOR_ID nvarchar(32) not null,
    VIEW_XML text not null,
    SNAPSHOT_XML text not null,
    SNAPSHOT_DATE timestamp not null,
    --
    primary key (ID),
    constraint FK_SYS_ENTITY_SNAPSHOT_AUTHOR_ID foreign key (AUTHOR_ID) references SEC_USER(ID)
)^

create index IDX_SYS_ENTITY_SNAPSHOT_ENTITY_ID on SYS_ENTITY_SNAPSHOT (ENTITY_ID)^
create index IDX_SYS_ENTITY_SNAPSHOT_SENTITY_ID on SYS_ENTITY_SNAPSHOT (STRING_ENTITY_ID)^
create index IDX_SYS_ENTITY_SNAPSHOT_IENTITY_ID on SYS_ENTITY_SNAPSHOT (INT_ENTITY_ID)^
create index IDX_SYS_ENTITY_SNAPSHOT_LENTITY_ID on SYS_ENTITY_SNAPSHOT (LONG_ENTITY_ID)^



create table SYS_CATEGORY(
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    DELETE_TS_NN timestamp not null default '1000-01-01 00:00:00.000',
    --
    NAME nvarchar(190) not null,
    SPECIAL nvarchar(50),
    ENTITY_TYPE nvarchar(100) not null,
    IS_DEFAULT varchar(1),
    DISCRIMINATOR integer,
    LOCALE_NAMES nvarchar(1000),
    --
    primary key (ID)
)^

create unique index IDX_SYS_CATEGORY_UNIQ_NAME_ENTITY_TYPE on SYS_CATEGORY (NAME, ENTITY_TYPE, DELETE_TS_NN)^



create table SYS_CATEGORY_ATTR (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    CATEGORY_ENTITY_TYPE text,
    NAME nvarchar(255),
    CODE nvarchar(100) not null,
    CATEGORY_ID nvarchar(32) not null,
    ENTITY_CLASS nvarchar(500),
    DATA_TYPE nvarchar(200),
    DEFAULT_STRING text,
    DEFAULT_INT integer,
    DEFAULT_DOUBLE numeric(36,6),
    DEFAULT_DATE timestamp,
    DEFAULT_DATE_IS_CURRENT varchar(1),
    DEFAULT_BOOLEAN varchar(1),
    DEFAULT_ENTITY_VALUE nvarchar(36),
    DEFAULT_STR_ENTITY_VALUE nvarchar(255),
    DEFAULT_INT_ENTITY_VALUE integer,
    DEFAULT_LONG_ENTITY_VALUE bigint,
    ENUMERATION nvarchar(500),
    ORDER_NO integer,
    SCREEN nvarchar(255),
    REQUIRED varchar(1),
    LOOKUP varchar(1),
    TARGET_SCREENS text,
    WIDTH nvarchar(20),
    ROWS_COUNT integer,
    IS_COLLECTION varchar(1),
    JOIN_CLAUSE text,
    WHERE_CLAUSE text,
    FILTER_XML text,
    LOCALE_NAMES nvarchar(1000),
    ENUMERATION_LOCALES text,
    --
    primary key (ID),
    constraint SYS_CATEGORY_ATTR_CATEGORY_ID foreign key (CATEGORY_ID) references SYS_CATEGORY(ID)
)^



create table SYS_ATTR_VALUE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    CATEGORY_ATTR_ID nvarchar(32) not null,
    ENTITY_ID nvarchar(32),
    STRING_ENTITY_ID nvarchar(190),
    INT_ENTITY_ID integer,
    LONG_ENTITY_ID bigint,
    STRING_VALUE text,
    INTEGER_VALUE integer,
    DOUBLE_VALUE numeric(36,6),
    DATE_VALUE timestamp,
    BOOLEAN_VALUE varchar(1),
    ENTITY_VALUE nvarchar(36),
    STRING_ENTITY_VALUE nvarchar(190),
    INT_ENTITY_VALUE integer,
    LONG_ENTITY_VALUE bigint,
    CODE nvarchar(100),
    PARENT_ID nvarchar(32),
    --
    primary key (ID),
    constraint SYS_ATTR_VALUE_CATEGORY_ATTR_ID foreign key (CATEGORY_ATTR_ID) references SYS_CATEGORY_ATTR(ID),
    constraint SYS_ATTR_VALUE_ATTR_VALUE_PARENT_ID foreign key (PARENT_ID) references SYS_ATTR_VALUE(ID)

)^

create index IDX_SYS_ATTR_VALUE_ENTITY on SYS_ATTR_VALUE(ENTITY_ID)^
create index IDX_SYS_ATTR_VALUE_SENTITY on SYS_ATTR_VALUE(STRING_ENTITY_ID)^
create index IDX_SYS_ATTR_VALUE_IENTITY on SYS_ATTR_VALUE(INT_ENTITY_ID)^
create index IDX_SYS_ATTR_VALUE_LENTITY on SYS_ATTR_VALUE(LONG_ENTITY_ID)^



create table SYS_JMX_INSTANCE (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    NODE_NAME nvarchar(255),
    ADDRESS nvarchar(500) not null,
    LOGIN nvarchar(50) not null,
    PASSWORD nvarchar(255) not null,
    --
    primary key (ID)
)^



create table SYS_QUERY_RESULT (
    ID bigint not null GENERATED BY DEFAULT AS IDENTITY,
    SESSION_ID nvarchar(32) not null,
    QUERY_KEY integer not null,
    ENTITY_ID nvarchar(32),
    STRING_ENTITY_ID nvarchar(190),
    INT_ENTITY_ID integer,
    LONG_ENTITY_ID bigint,
    --
    primary key (ID)
)^

create index IDX_SYS_QUERY_RESULT_ENTITY_SESSION_KEY on SYS_QUERY_RESULT (ENTITY_ID, SESSION_ID, QUERY_KEY)^
create index IDX_SYS_QUERY_RESULT_SENTITY_SESSION_KEY on SYS_QUERY_RESULT (STRING_ENTITY_ID, SESSION_ID, QUERY_KEY)^
create index IDX_SYS_QUERY_RESULT_IENTITY_SESSION_KEY on SYS_QUERY_RESULT (INT_ENTITY_ID, SESSION_ID, QUERY_KEY)^
create index IDX_SYS_QUERY_RESULT_LENTITY_SESSION_KEY on SYS_QUERY_RESULT (LONG_ENTITY_ID, SESSION_ID, QUERY_KEY)^

create index IDX_SYS_QUERY_RESULT_SESSION_KEY on SYS_QUERY_RESULT (SESSION_ID, QUERY_KEY)^



create table SEC_REMEMBER_ME (
    ID nvarchar(32),
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    VERSION integer,
    --
    USER_ID nvarchar(32) not null,
    TOKEN nvarchar(32) not null,
    --
    primary key (ID),
    constraint FK_SEC_REMEMBER_ME_USER foreign key (USER_ID) references SEC_USER(ID)
)^
create index IDX_SEC_REMEMBER_ME_USER on SEC_REMEMBER_ME(USER_ID)^
create index IDX_SEC_REMEMBER_ME_TOKEN on SEC_REMEMBER_ME(TOKEN)^



create table SYS_ACCESS_TOKEN (
    ID nvarchar(32) not null,
    CREATE_TS timestamp,
    --
    TOKEN_VALUE nvarchar(255),
    TOKEN_BYTES blob,
    AUTHENTICATION_KEY nvarchar(255),
    AUTHENTICATION_BYTES blob,
    EXPIRY timestamp,
    USER_LOGIN nvarchar(50),
    LOCALE nvarchar(200),
    REFRESH_TOKEN_VALUE nvarchar(255),
    --
    primary key (ID)
)^



create table SYS_REFRESH_TOKEN (
    ID nvarchar(32) not null,
    CREATE_TS timestamp,
    --
    TOKEN_VALUE nvarchar(255),
    TOKEN_BYTES blob,
    AUTHENTICATION_BYTES blob,
    EXPIRY timestamp,
    USER_LOGIN nvarchar(50),
    --
    primary key (ID)
)^



create table SEC_SESSION_LOG (
    ID nvarchar(32) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY nvarchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY nvarchar(50),
    DELETE_TS timestamp,
    DELETED_BY nvarchar(50),
    --
    SESSION_ID nvarchar(32) not null,
    USER_ID nvarchar(32) not null,
    SUBSTITUTED_USER_ID nvarchar(32),
    USER_DATA text,
    LAST_ACTION integer not null,
    CLIENT_INFO nvarchar(512),
    CLIENT_TYPE nvarchar(10),
    ADDRESS nvarchar(255),
    STARTED_TS timestamp,
    FINISHED_TS timestamp,
    SERVER_ID nvarchar(128),
    --
    primary key (ID),
    constraint FK_SESSION_LOG_ENTRY_USER foreign key (USER_ID) references SEC_USER(ID),
    constraint FK_SESSION_LOG_ENTRY_SUBUSER foreign key (SUBSTITUTED_USER_ID) references SEC_USER(ID)
)^

create index IDX_SESSION_LOG_ENTRY_USER on SEC_SESSION_LOG (USER_ID)^
create index IDX_SESSION_LOG_ENTRY_SUBUSER on SEC_SESSION_LOG (SUBSTITUTED_USER_ID)^
create index IDX_SESSION_LOG_ENTRY_SESSION on SEC_SESSION_LOG (SESSION_ID)^
create index IDX_SESSION_LOG_STARTED_TS on SEC_SESSION_LOG (STARTED_TS DESC)^



















insert into SEC_GROUP (ID, CREATE_TS, VERSION, NAME, PARENT_ID)
values ('0fa2b1a51d684d699fbddff348347f93', current_timestamp, 0, 'Company', null)^

insert into SEC_USER (ID, CREATE_TS, VERSION, LOGIN, LOGIN_LC, PASSWORD, NAME, GROUP_ID, ACTIVE)
values ('608859871b61424794c7dff348347f93', current_timestamp, 0, 'admin', 'admin',
'cc2229d1b8a052423d9e1c9ef0113b850086586a',
'Administrator', '0fa2b1a51d684d699fbddff348347f93', 1)^

insert into SEC_USER (ID, CREATE_TS, VERSION, LOGIN, LOGIN_LC, PASSWORD, NAME, GROUP_ID, ACTIVE)
values ('a405db59e6744f638afe269dda788fe8', now(), 0, 'anonymous', 'anonymous', null,
'Anonymous', '0fa2b1a51d684d699fbddff348347f93', 1)^

insert into SEC_ROLE (ID, CREATE_TS, VERSION, NAME, ROLE_TYPE)
values ('0c018061b26f4de2a5bedff348347f93', current_timestamp, 0, 'Administrators', 10)^

insert into SEC_ROLE (ID, CREATE_TS, VERSION, NAME, ROLE_TYPE)
values ('cd541dd4eeb7cd5b847ed32236552fa9', current_timestamp, 0, 'Anonymous', 30)^

insert into SEC_USER_ROLE (ID, CREATE_TS, VERSION, USER_ID, ROLE_ID)
values ('c838be0a96d04ef4a7c0dff348347f93', current_timestamp, 0, '608859871b61424794c7dff348347f93', '0c018061b26f4de2a5bedff348347f93')^

insert into SEC_USER_ROLE (ID, CREATE_TS, VERSION, USER_ID, ROLE_ID)
values ('f01fb532c2f0dc18b86c450cf8a8d8c5', current_timestamp, 0, 'a405db59e6744f638afe269dda788fe8', 'cd541dd4eeb7cd5b847ed32236552fa9')^

insert into SEC_FILTER (ID,CREATE_TS,CREATED_BY,VERSION,UPDATE_TS,UPDATED_BY,DELETE_TS,DELETED_BY,COMPONENT,NAME,XML,USER_ID,GLOBAL_DEFAULT)
values ('b61d18cbe79a46f3b16deaf4aebb10dd',{ts '2010-03-01 11:14:06.830'},'admin',2,{ts '2010-03-01 11:52:53.170'},'admin',null,null,'[sec$User.browse].genericFilter','Search by role',
'<?xml version="1.0" encoding="UTF-8"?>
<filter>
  <and>
    <c name="UrMxpkfMGn" class="com.haulmont.cuba.security.entity.Role" type="CUSTOM" locCaption="Role" entityAlias="u" join="join u.userRoles ur">ur.role.id = :component$genericFilter.UrMxpkfMGn32565
      <param name="component$genericFilter.UrMxpkfMGn32565">NULL</param>
    </c>
  </and>
</filter>',
'608859871b61424794c7dff348347f93',0)^


create table SYS_SEQUENCE (
    NAME nvarchar(100) not null,
    INCREMENT int not null default 1,
    CURR_VALUE bigint default 0,
    primary key (NAME)
)^