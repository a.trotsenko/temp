# 0. Install HANA Express (server + client)

# 1. build cuba-hana-support
```
1.1. cd cuba-hana-support
1.2. gradlew build install
```

# 2. Set up cuba-app:
2.1. edit `app.properties`:
```
cuba.dbmsType = hana  
cuba.automaticDatabaseUpdate = true
```

2.2. edit `build.gradle`:
add maven local repo
```
buildscript {
    ...
    repositories {
        mavenLocal()
        ...
    }
    ...
}
```

modify existing 'configure(globalModule)' block:
```
configure(globalModule) {
    dependencies {
        compile("com.sap.cloud.db.jdbc:ngdbc:2.3.53")
    }
    ...
}
```

then add new block:
```
configure(coreModule) {
    dependencies {
        compile("com.company:cuba-hana-support:0.1-SNAPSHOT")
    }
}
def hana = 'com.sap.cloud.db.jdbc:ngdbc:2.3.53'
```

then modify origin 'configure(coreModule)' block:
```
configure(coreModule) {
    org.apache.tools.ant.Project.class.classLoader.addURL( file('c:/dev/sap/hdbclient/ngdbc.jar').toURI().toURL() )
    
    configurations {
        jdbc
        dbscripts
    }
    ...
    dependencies {
        compile(globalModule)
        provided(servletApi)
        jdbc(hana) // replace hsql to hana
        testRuntime(hana) // replace hsql to hana
    }
    ...
    task deploy(dependsOn: [assemble, cleanConf], type: CubaDeployment) {
        ...
        appJars(modulePrefix + '-global', modulePrefix + '-core', 'cuba-hana-support') // add 'cuba-hana-support'
    }
    
    def HANA_DB_NAME = 'CUBADB'
    def HANA_DBMS_NAME = 'hana'
    def HANA_DB_USER = 'system'
    def HANA_DB_PASS = 'hanaPAZZ1'
    def HANA_TIMESTAMP_TYPE = 'TIMESTAMP'
 
    task createDb(dependsOn: assembleDbScripts, description: 'Creates local database', type: CubaDbCreation) {
        dbms = HANA_DBMS_NAME
        driver = 'com.sap.db.jdbc.Driver'
        dbUrl = 'jdbc:sap://hxehost:39013/?databasename=' + HANA_DB_NAME
        masterUrl = 'jdbc:sap://hxehost:39013'

        dbUser = HANA_DB_USER
        dbPassword = HANA_DB_PASS

        dropDbSql = 'ALTER SYSTEM STOP DATABASE ' + HANA_DB_NAME + ';\n' +
                'DROP DATABASE ' + HANA_DB_NAME + ';'

        createDbSql = 'CREATE DATABASE ' + HANA_DB_NAME + ' ADD \'scriptserver\' SYSTEM USER PASSWORD ' + HANA_DB_PASS + ';\n' +
                'ALTER SYSTEM START DATABASE ' + HANA_DB_NAME + ';'

        timeStampType = HANA_TIMESTAMP_TYPE

    }

    task updateDb(dependsOn: assembleDbScripts, description: 'Updates local database', type: CubaDbUpdate) {
        dbms = HANA_DBMS_NAME
        driver = 'com.sap.db.jdbc.Driver'
        dbUrl = 'jdbc:sap://hxehost:39013/?databasename=' + HANA_DB_NAME
        dbUser = HANA_DB_USER
        dbPassword = HANA_DB_PASS
        timeStampType = HANA_TIMESTAMP_TYPE
    }
}
```

2.3 add DS resource to **modules/core/web/META-INF/context.xml**:
```
<Resource
      name="jdbc/CubaDS"
      type="javax.sql.DataSource"
      maxTotal="20"
      maxIdle="2"
      maxWaitMillis="5000"
      driverClassName="com.sap.db.jdbc.Driver"
      url="jdbc:sap://hxehost:39013?databasename=CUBADB"
      username="system"
      password="hanaPAZZ1"/>
```
2.4 copy `00.create-db.sql` to **modules/core/db/init/hana**